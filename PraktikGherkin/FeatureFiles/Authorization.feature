﻿Feature: Autorization
	As a client
	I want to have access to my account
	In ordet to get information about order history


Scenario: Unauthorized user is authorized with login and password on the website
	Given Allo website is open
	Given User is not logged in
	Given User is registered 
	When User clicks on Login
	When User enter Phone or Email in the field
	When User enter password in the field
	When User click on button Enter
	Then User authorized on the website


	Scenario: Unauthorized user is authorized with gmail account on the website
	Given Allo website is open
	Given User is not logged in
	Given User is registered with gmail account
	When User clicks the button Login
	When User clicks the button google
	When User clicks the button with registered gmail
    Then User authorized on the website