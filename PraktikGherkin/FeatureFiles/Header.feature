﻿Feature: Header
	As a user
	I want to have header with website main elements
	In order to have quick acces to main pages


Scenario: Authorized user enter to blog page
	Given Allo website is open
	Given User authorized on Allo website
	When User clicks Blog
	Then User on Blog page
	But User don't see Header on Blog page

	Scenario: Authorized user enter to fishka page
	Given Allo website is open
	Given User authorized on Allo website
	When User clicks Fishka
	Then User on Fishka page
	