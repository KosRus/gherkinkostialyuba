﻿using System;
using TechTalk.SpecFlow;

namespace PraktikGherkin.Steps
{
    [Binding]
    public class HeaderSteps
    {
        [Given(@"User authorized on Allo website")]
        public void GivenUserAuthorizedOnAlloWebsite()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks Blog")]
        public void WhenUserClicksBlog()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks Fishka")]
        public void WhenUserClicksFishka()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"User on Blog page")]
        public void ThenUserOnBlogPage()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"User don't see Header on Blog page")]
        public void ThenUserDonTSeeHeaderOnBlogPage()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"User on Fishka page")]
        public void ThenUserOnFishkaPage()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
