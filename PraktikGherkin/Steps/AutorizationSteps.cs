﻿using System;
using TechTalk.SpecFlow;

namespace PraktikGherkin.Steps
{
    [Binding]
    public class AutorizationSteps
    {
        [Given(@"User is registered")]
        public void GivenUserIsRegistered()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"User is registered with gmail account")]
        public void GivenUserIsRegisteredWithGmailAccount()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks on Login")]
        public void WhenUserClicksOnLogin()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User enter Phone or Email in the field")]
        public void WhenUserEnterPhoneOrEmailInTheField()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User enter password in the field")]
        public void WhenUserEnterPasswordInTheField()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User click on button Enter")]
        public void WhenUserClickOnButtonEnter()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks the button Login")]
        public void WhenUserClicksTheButtonLogin()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks the button google")]
        public void WhenUserClicksTheButtonGoogle()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks the button with registered gmail")]
        public void WhenUserClicksTheButtonWithRegisteredGmail()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"User authorized on the website")]
        public void ThenUserAuthorizedOnTheWebsite()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
